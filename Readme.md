
# Sistema reserva de ticket

El trabajo practico se basa en crear una reserva de ticket a cierto eventos, los usuarios podrán registrarse e ingresar sesión.

## Requisitos

- [Docker](https://www.docker.com/get-started/)
- [Node.Js](https://nodejs.org/es)
- [DBeaver](https://dbeaver.io/)
- [Postman](https://www.postman.com/)
- [GitLab](https://about.gitlab.com/)

## Puntos a de desarrollar

    - Codigo en GitLab Readme ✔️
    - JSON de postman ✔️
    - Video demo 10 minutos ✔️
    Deseable
    - Documentación con swagger ✔️
    - relaciones ❌   

## Libreria utilizadas
- express
- nodemon
- morgan
- jsonwebtoken
- bcrypt
- cookie-parser
- cors
- mysql
- typeorm
- swagger
- postman

## uso del sistema

El uso de swagger para documentar la api

Se utilizo doker para la base de datos con MySQL

El sistema de ticket cuenta con tres permisos, los cuales son:
    - Anonimo
    - Cliente
    - Administrador



### El anonimo
La persona que son agenas al sistema permiten registrarse o logguearse en la API.
- Anonimo:
    - se registra enviando el username y password
    - Iniciará sesión enviando el username y password
    - Cerrara sesión permitiendo que no hayas datos en cookies
    . Visualizará todo los eventos disponibles.

### El cliente
La persona cuando se registrán en el sistema obtiene el permiso "client" el cual tendrá algunos priviligios en la API
- User: 
    - El cliente puede modificar su nombre de usuario
    - El cliente podrá eliminar su cuenta del sistema 
- Eventos: 
    - El cliente puede ver todos los eventos que ofrece el sistema
    - El cliente puede ver un evento en el sistema
-Bookings:
    - El cliente podrá solicitar un ticket, en el sistema tiene que enviar el nombre del evento y cantidad de personas.
    - El cliente podrá ver los ticket que compro en el evento
    - El cliente podrá modificar la cantidad de personas para el evento, enviando el id del evento y las personas.
    - El cliente se podrá bajar del evento, pasar el id del evento que se quiera dar de baja
### El administrador
La personas administradora se puede modificar desde la base de datos, en la opción "permission"
- User:
    - El administrador podrá ver los clientes que están en el sistema.
    - El administrador podrá cambiar el permiso a los usuarios.
- Events:
    - El administrador puede crear un evento nuevo.
    - El administrador puede modificar un evento nuevo.
    - El administrador puede eliminar un evento nuevo.

[Ver el video demostrando como funciona la API](https://www.veed.io/view/f41dfa8b-1826-4d68-989e-38f57f3a6d6b?panel=share)



