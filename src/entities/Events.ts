import { BaseEntity, Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Bookings } from "./Bookings";

@Entity()
export class Events extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({unique: true})
    nombre: string

    @Column()
    descripcion: string

    @Column()
    lugar: string

    @Column()
    fechaHora: Date

    @Column()
    gps: string

    @Column()
    precio: number

    @Column({nullable: true})
    limite: number

    @Column({default: 0})
    ventas: number
    
    @Column()
    tipoEvento: string

    @OneToOne(() => Bookings)
    bookings: Bookings
}