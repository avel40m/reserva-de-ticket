import {Entity,BaseEntity,PrimaryGeneratedColumn,Column} from 'typeorm';
import { User } from './User';
import { Events } from './Events';

@Entity()
export class Bookings extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    total: number
    @Column()
    personas: number
    @Column()
    eventsId: number
    @Column()
    userId: number
}