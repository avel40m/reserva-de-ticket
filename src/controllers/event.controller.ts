import { Request, Response } from "express"
import { Events } from "../entities/Events"

export const getEvents = async (req:Request,res:Response) => {
    try {
        const events = await Events.find();
        res.status(200).json(events);
    } catch (error) {
        res.status(500).json({message: error})
    }
}

export const getEvent = async (req: Request,res:Response) => {
    const id = req.params.id    
    try {
        if(!id || id == null) return res.status(404).json({message:"Id is required"});
        const event = await Events.findOneBy({id: parseInt(id)});
        if(!event) return res.status(404).json({message: 'Event not found'});
        
        res.status(200).json(event);
    } catch (error) {
        res.status(500).json({message:error});
    }
}

export const postEvent = async (req:Request,res:Response) => {
    try {
        const newEvent = await Events.save(req.body);
        res.status(201).json(newEvent);

    } catch (error) {
        res.status(500).json({message: error})
    }
}

export const updateEvent = async (req:Request,res:Response) => {
    const id = req.params.id
    try {
        if(!id) return res.status(404).json({message:"Id is required"});
        const searchEvent = await Events.findOneBy({id: parseInt(id)});
        if(!searchEvent) return res.status(404).json({message: 'Event not found'});
        
        await Events.update({id:parseInt(id)},req.body);
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json({message:error});
    }
}

export const deleteEvent = async (req:Request,res:Response) => {
    const id = req.params.id
    try {
        if(!id) return res.status(404).json({message:"Id is required"});
        const searchEvent = await Events.findOneBy({id: parseInt(id)});
        if(!searchEvent) return res.status(404).json({message: 'Event not found'});

        await Events.delete({id: parseInt(id)});
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json({message: error})
    }
} 