import { Request, Response } from "express";
import { Events } from "../entities/Events";
import { Bookings } from "../entities/Bookings";
import { User } from "../entities/User";

export const postBooking = async (req: Request,res:Response) => {
    const {nombre,personas} = req.body; 
    try {
        const id = req.user        
        if(!id) return res.status(404).json({message: 'user not logged in'});
        const event = await Events.findOneBy({nombre: nombre});
        if(!event) return res.status(404).json({message:'Event not found'});       
        if(event.limite < (event.ventas + personas)) return res.status(404).json({message:`No place, number of tickets ${event.limite - event.ventas}`});
        
        await Events.update({id: event.id},{...event,ventas: event.ventas + personas})
        const booking = new Bookings();
        booking.personas = personas;
        booking.total = event.precio * personas
        booking.eventsId = event.id;
        booking.userId = id
        await booking.save();
        console.log(booking);
        
        res.status(201).json([{
            message: 'Ticket sould',
            evento: event.nombre,
            personas: booking.personas,
            precio: event.precio,
            total: booking.total,
            "dia de compra": new Date().toDateString(),
            "dia del evento": event.fechaHora.toDateString()
        }
        ])

    } catch (error) {
        res.status(500).json(error);
    }
}

export const getMyBookings = async (req: Request, res:Response) => {
    try {
        const id = req.user
        if(!id) return res.status(404).json({message: 'user not logged in'});
        const tickets = [];

        const booking = await Bookings.find(
            {where: {userId: id}
        });
        
        for (let i = 0; i < booking.length; i++) {
            const event = await Events.find({where: {id: booking[i].eventsId}});            
            
            const obj = {
                id:booking[i].id,
                nombre:event[0].nombre,
                descripcion:event[0].descripcion,
                lugar:event[0].lugar,
                horario:event[0].fechaHora,
                ubicacion:event[0].gps,
                precio:event[0].precio,
                total:booking[i].total,
                personas:booking[i].personas,
            }
            tickets.push(obj)
        }
        if(tickets.length == 0) return res.status(200).json({message:"Does not have tickets"})
        const user = await User.findOneBy({id: booking[0].userId})
        res.status(200).json({
            usuario: user?.username,
            tickets: tickets
        });        
    } catch (error) {
        res.status(500).json(error);
    }
}

export const updateBooking = async (req:Request,res:Response) => {
    const id = req.params.id;
    const { personas } = req.body;
    try {
        const userid = req.user
        if(!userid) return res.status(404).json({message: 'user not logged in'});
        const bookingUser = await Bookings.find({where: {userId: userid}});
        if(!bookingUser) return res.status(404).json({message: "you have no tickets purchased"});
        
        const booking = bookingUser.filter(event => event.id == parseInt(id));        
        
        if(booking.length == 0) return res.status(404).json({message: "do not have a ticket for the event"});

        const events = await Events.find({ where: {id: booking[0].eventsId}});
        if(!events) return res.status(404).json({message: "Event not found"});
        
        const result = events[0].ventas - booking[0].personas + personas;
                
        if(events[0].limite < result) return res.status(404).json({message:`No place, number of tickets ${events[0].limite - (events[0].ventas - bookingUser[0].personas)}`});
          
        await Bookings.update(booking[0].id,{...booking[0],personas: personas,total: events[0].precio * personas})
        await Events.update(events[0].id,{...events[0],ventas: result}) 

        res.status(200).json([{
            message: "Events update successfully",
            nombre: events[0].nombre,
            lugar: events[0].lugar,
            personas: personas,
            total: events[0].precio * personas
        }]);
    } catch (error) {
        res.status(500).json(error);
    }
}

export const deleteBooking = async (req: Request,res:Response) => {
    const id = req.params.id;
    try {
        const userid = req.user
        if(!userid) return res.status(404).json({message: 'user not logged in'});
        const eventsUser = await Bookings.find({where: {userId: userid}});
        if(!eventsUser) return res.status(404).json({message: "you have no tickets purchased"});
        
        const booking = eventsUser.filter(event => event.id == parseInt(id));        
        
        if(booking.length == 0) return res.status(404).json({message: "do not have a ticket for the event"});

        const events = await Events.find({ where: {id: booking[0].eventsId}});
        if(!events) return res.status(404).json({message: "Event not found"});

        await Events.update({id: events[0].id},{...events[0], ventas: events[0].ventas - booking[0].personas})
        await Bookings.delete(id); 
        
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json(error);
    }
} 