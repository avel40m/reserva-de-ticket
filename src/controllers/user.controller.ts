import {Request,Response} from 'express';
import { User } from '../entities/User';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

export const signUp = async(req:Request,res:Response) => {
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({message: "The username or password not have empty"});
    }
    const user = await User.findOneBy({username: req.body.username});
    if (user) {
        return res.status(404).json({message: "The username already exist"})
    }
    const newUser = new User();
    newUser.username = req.body.username;
    newUser.password = await createHash(req.body.password);
    await newUser.save();
    res.status(201).json(newUser);
}

export const signIn = async (req:Request,res:Response) => {
    if (!req.body.username || !req.body.password) {
        return res.status(404).json({message: "The username or password not have empty"});
    }
    const user = await User.findOneBy({username: req.body.username});
    if (!user) {
       return res.status(404).json({message:"The username not already exist."}) 
    }
    const isMatch = await comparePassword(user,req.body.password);
    if (!isMatch) {
       return res.status(404).json({message:"The password is incorrect."}) 
    }
    return res.status(200).cookie('credentials',createToken(user)).json({message: "successfully"});

}

export const signout = (req: Request,res:Response) => {
    res.status(200).cookie('credentials','',{expires: new Date(0)}).json({message:"session closed"})
}

export const getUsers = async (req: Request,res:Response) => {
    try {
        const users = await User.find();        
        res.status(200).json(users);
    } catch (error) {
        return res.status(500).json(error);
    }
}

export const getUser = async (req:Request, res:Response) => {
    try {
        const id = req.user
        if(!id) return res.status(404).json({message: 'user not logged in'});
        const user = await User.findOneBy({id: id});
        res.status(200).json(user);
    } catch (error) {
        res.status(500).json(error);
    }
}

export const updateUser = async (req: Request,res: Response) => {
    try {
        const id = req.user
        if(!id) return res.status(404).json({message: 'user not logged in'});
        await User.update({id:parseInt(id)}, req.body);
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json(error);
    }
}

export const deleteUser = async (req:Request,res:Response) => {
    try {
        const result = await User.delete({id: req.user});
        if(result.affected === 0) return res.status(404).json({message: 'User not found'});
        
        res.cookie('credentials','',{expires: new Date(0)}).sendStatus(204);
    } catch (error) {
        res.status(500).json(error)
    }
}

export const givePermission = async (req: Request,res: Response) => {
    const { id } = req.params;
    try {
        const user = await User.findOneBy({id: parseInt(id)});
        if(!user) return res.status(404).json({message:"User not found"});
        await User.update({id: parseInt(id)},req.body);

        res.sendStatus(204);
    } catch (error) {
        res.status(500).json(error);
    }
}
 
const createHash = async(password:string): Promise<string> => {
    const saltRounds = 10;
    return await bcrypt.hash(password,saltRounds);
}

const comparePassword = async (user:User, password:string): Promise<Boolean> => {
    return await bcrypt.compare(password,user.password);
}

const createToken = (user:User) => {
    const token = jwt.sign({id: user.id,permission:user.permission},'clavesecret',{expiresIn:'1h'});

    return token;
}