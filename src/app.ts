import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import userRoutes from './routes/user.routes';
import eventsRoutes from './routes/event.routes';
import bookingsRoutes from './routes/bookings.routes';
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc'
import { options } from './swaggerOptions';

const app = express();

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(cookieParser());


const specs = swaggerJsDoc(options);
app.use("/docs",swaggerUi.serve,swaggerUi.setup(specs));

app.use("/api", userRoutes);
app.use("/api", eventsRoutes);
app.use("/api", bookingsRoutes);

export default app;