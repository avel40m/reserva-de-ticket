import { NextFunction, Request, Response } from "express";
import jwt, { JwtPayload } from 'jsonwebtoken';

interface DecodedToken extends JwtPayload {
    id: number,
    permissions: string[]
}

declare global {
    namespace Express {
        interface Request {
            user?: any;
        }
    }
}

export const validateTokenAdmin = async (req:Request,res: Response,next: NextFunction) => {
    const token = req.cookies.credentials;
    if(!token) return res.status(401).json({message: 'Token not found!'});
    try {
        const {id, permission } = jwt.verify(token,'clavesecret') as DecodedToken;          
        if(permission != 'admin') return res.status(403).json({message:'Does not have permission'})
        
        next();
    } catch (error) {
        return res.status(401).json({message: 'Token is invalid'});
    }
}

export const validateTokenClient = async (req:Request,res: Response,next: NextFunction) => {
    const token = req.cookies.credentials;
    if(!token) return res.status(401).json({message: 'Token not found!'});
    try {
        const { id,permission } = jwt.verify(token,'clavesecret') as DecodedToken;  
       
        if(permission != 'client') return res.status(403).json({message:'Does not have permission'})
        req.user = id;
        next();
    } catch (error) {
        return res.status(401).json({message: 'Token is invalid'});
    }
}