export const options = {
    definition: {
    openapi: "3.0.0",
    info: {
     title: "Sistema de ticket",
     version: "1.0.0",
     description: "Un simple sistema de ticket para poder ingresar a eventos",
     contact:{
         name: "Mendez avelino",
         url: "https://www.linkedin.com/in/avelino-mendez/"
     }
    },
    servers: [
    {
     url: "http://localhost:3000",
    },
    ],
    },
    apis: [`${__dirname}/docs/**/*.yaml`],
    };
    