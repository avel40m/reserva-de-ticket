import {DataSource} from 'typeorm';
import { User } from './entities/User';
import { Events } from './entities/Events';
import { Bookings } from './entities/Bookings';

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3307,
    username: "root",
    password: "mysql",
    database: "reserva-ticket",
    synchronize: true,
    entities: [User, Events,Bookings]
})