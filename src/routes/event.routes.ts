import {Router} from 'express';
import {deleteEvent, getEvent, getEvents, postEvent, updateEvent} from '../controllers/event.controller';
import { validateTokenAdmin } from '../middlewares/validate.token';

const router = Router();

router.get('/events', getEvents);

router.get('/event/:id', getEvent);

router.post('/event',validateTokenAdmin ,postEvent);

router.put('/event/:id', validateTokenAdmin, updateEvent);

router.delete('/event/:id', validateTokenAdmin, deleteEvent);

export default router;