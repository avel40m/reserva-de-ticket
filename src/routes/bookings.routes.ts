import {Router} from 'express';
import { deleteBooking, getMyBookings, postBooking, updateBooking } from '../controllers/bookings.controller';
import { validateTokenClient } from '../middlewares/validate.token';
const router = Router();

router.post('/booking',validateTokenClient,postBooking);

router.get('/booking', validateTokenClient,getMyBookings);

router.put('/booking/:id', validateTokenClient, updateBooking);

router.delete('/booking/:id', validateTokenClient, deleteBooking);

export default router;