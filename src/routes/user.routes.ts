import {Router} from 'express';
import { deleteUser, getUser, getUsers, givePermission, signIn, signUp, signout, updateUser } from '../controllers/user.controller';
import { validateTokenAdmin, validateTokenClient } from '../middlewares/validate.token';

const router = Router();

router.get('/users',validateTokenAdmin ,getUsers);

router.put('/user/:id', validateTokenAdmin, givePermission);

router.get('/user', validateTokenClient, getUser);

router.put('/user', validateTokenClient, updateUser);

router.delete('/user', validateTokenClient, deleteUser);

router.post('/signup', signUp)

router.post('/signin', signIn)

router.post('/signout', signout);

export default router;